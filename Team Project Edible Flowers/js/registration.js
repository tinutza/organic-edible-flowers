// JavaScript Document
// Name and Password from the register-form
$(document).ready(function() {
    var menu;
    /* Toggle menu*/
    $(function() {
        var pull = $('#pull');
        menu = $('nav ul');

        $(pull).on('click', function(e) {
            e.preventDefault();
            menu.slideToggle();
        });
    });
    //Resize the window when the breakpoint is hit
    $(window).resize(function() {
        var w = $(window).width();
        if (w > 768 && menu.is(':hidden')) {
            menu.removeAttr('style');
        }
    });
    //Global variable to hold the stored first Name
    var storedFirstName = "";

    //Make the error messages of the validation invisble on load
    $('.registrationError').css("visibility", "hidden");

    //Submit the sign up form
    $('#registrationForm').submit(function(event) {

        var firstName = $('#firstName').val();
        var lastName = $('#lastName').val();
        var pw = $('#password').val();
        var repeatpw = $('#reppassword').val();
        var email = $('#email').val();
        var avatarName = "empty_avatar.jpg";

        //Check if an avatar was chosen store the name of the file
        if ($('#uploadAvatar')[0].files[0] != undefined || $('#uploadAvatar')[0].files[0] != null || $('#uploadAvatar')[0].files[0] != "") {
           avatarName = $('#uploadAvatar')[0].files[0].name;
        }

        //validate input
        var notValid = true;
        if (firstName.length < 2 || lastName.length < 2) {
            notValid = false;
        }
        if (firstName.length < 2 || lastName.length < 2 || email === "" || pw.length < 6 || repeatpw.length < 6) {
            notValid = false;
        }
        if (pw !== repeatpw) {
            notValid = false;
        }
        if (!(/^[a-zA-z_\-.]+@[a-z]+\.[a-z]{1,}$/.test(email))) {
            notValid = false;
        }
        $('#registrationForm input').each(function() {
            if (!isValid($(this))) {
                $(this).parent().siblings('p').css("visibility", "visible");
                event.preventDefault();
            } else {
                $(this).parent().siblings('p').css("visibility", "hidden");

            }

            localStorage.setItem('firstName', firstName);
            localStorage.setItem('lastName', lastName);
            localStorage.setItem('email', email);
            localStorage.setItem('pw', pw);
            localStorage.setItem('avatarName', avatarName);
        });
    });
    //Login form submition
    $('#loginForm').submit(function(event) {
        var storedEmail = localStorage.getItem('email');
        var storedPassword = localStorage.getItem('pw');
        var email = $('#emailLogin').val();
        var pw = $('#passwordLogin').val();

        // check if stored data from register-form is equal to data from login form
        if (email !== storedEmail || pw !== storedPassword || email === "") {
            alert('Email or Password are not correct or you must sign up first');
            event.preventDefault();
        }
    });
    storedFirstName = localStorage.getItem('firstName');
    if (storedFirstName === "" || storedFirstName == null) {
        $('#loginLink').show();
        $('#signOutLink').hide();
    } else {
        $('#loginLink').hide();
        $('#signOutLink').show();
    }
    $('#signOutLink').click(function() {

        var r = confirm(storedFirstName + ", are you sure you want to sign out?");
        if (r) {
            localStorage.clear();
            window.location.reload();
        }
    });

});
//Function to validate each of the input fields using regex
function isValid(origin) {
    var isValid = false;
    var patt = /^.+$/;
    var input = $(origin).val();

    switch ($(origin).attr("id")) {
        case "firstName":
        case "lastName":
            patt = /^[A-Za-z .-]{2,}$/;
            break;

        case "email":
            patt = /^[a-zA-z_-]+@[a-z]+\.[a-z]{1,}$/;
            break;
        case "password":
        case "reppassword":
            patt = /^.{6,}$/;
            break;

    }
    if (patt.test(input)) {
        isValid = true;
    }

    if ($(origin).attr("id") === "reppassword") {
        if ($(origin).val() !== $('#password').val()) {
            isValid = false;
        }
    }

    return isValid;
}